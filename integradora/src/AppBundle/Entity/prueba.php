<?php

namespace AppBundle\Entity;
    
use Doctrine\ORM\Mapping as ORM;
    
/** @ORM\Entity */
 class prueba{
    
     /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
     
     /**
     * @ORM\Column(type="string", length=20)
     */
    private $nombre;
     
     /**
     * @ORM\Column(type="string", length=20)
     */
    private $apellido;
     
     /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;
     
     /**
     * @ORM\Column(type="string", length=15)
     */
    private $profe;
     
     /**
     * @ORM\Column(type="integer")
     */
    private $sueldo;
    
    public function getId(){
        return $this->id;
    }
    
    public function setNombre ($nombre){
        $this->nombre = $nombre;
    }
    
    public function getNombre(){
        return $this->nombre;
    }
    
      public function setApellido ($apellido){
        $this->apellido = $apellido;
    }
    
    public function getApellido(){
        return $this->apellido;
    }
    
      public function setFecha ($fecha){
        $this->fecha = $fecha;
    }
    
    public function getFecha(){
        return $this->fecha;
    }
    
      public function setProfe ($profe){
        $this->profe = $profe;
    }
    
    public function getProfe(){
        return $this->profe;
    }
    
      public function setSueldo ($sueldo){
        $this->sueldo = $sueldo;
    }
    
    public function getSueldo(){
        return $this->sueldo;
    }
}